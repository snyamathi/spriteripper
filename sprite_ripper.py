from math import ceil
from math import sqrt
from PIL import Image

class ImageCutter():
    def __init__(self, image, cell_width, cell_height):
        if not isinstance(image, Image.Image):
            raise TypeError("first argument is not a PIL Image")
        elif not isinstance(cell_width, int):
            raise TypeError("second argument is not an integer")
        elif not isinstance(cell_height, int):
            raise TypeError("third argument is not an integer")
        elif image.size[0] % cell_width != 0:
            raise ValueError("cell_width does not divide evenly into image_width")
        elif image.size[1] % cell_height != 0:
            raise ValueError("cell_height does not divide evenly into image_height")
            
        self.image = image
        self.cell_width = cell_width
        self.cell_height = cell_height
        self.columns = image.size[0] / cell_width
        self.rows = image.size[1] / cell_height
        
    def cut(self):
        imageSet = ImageSet()
        for x in range(self.columns):
            for y in range(self.rows):
                left = x * self.cell_width
                upper = y * self.cell_height
                right = left + self.cell_width
                lower = upper + self.cell_width
                bounds = (left, upper, right, lower)
                imageSet.add(self.image.crop(bounds))
        
        return imageSet

class ImageSet():
    def __init__(self):
        self.data = []
        
    def add(self, arg):
        if not isinstance(arg, Image.Image):
            raise TypeError("argument is not a PIL Image")
        
        for image in self.data:
            if ImageSet.imagesAreEqual(image, arg):
                return False
                
        self.data.append(arg)
        return True
        
    def save_images(self, prefix=""):
        if not isinstance(prefix, str):
            raise TypeError("prefix is not a string")
            
        for index, image in enumerate(self.data):
            filename = str(index) + prefix + ".png"
            image.save(filename, "PNG")
            
    def create_spritesheet(self, filename):
        if not isinstance(filename, str):
            raise TypeError("filename is not a string")
            
        num_cells = int(ceil(sqrt(len(self.data))))
        cell_width = self.data[0].size[0]
        cell_height = self.data[0].size[1]
        
        sprite_sheet = Image.new("RGBA", (num_cells * cell_width, num_cells * cell_height), (255, 255, 255, 0))

        index = 0
        for y in range(num_cells):
            for x in range(num_cells):           
                sprite_sheet.paste(self.data[index], (x * cell_width, y * cell_height))
                index += 1
                if index == len(self.data):
                    break
        
   
        sprite_sheet.save(filename, "PNG")
        
    @staticmethod
    def imagesAreEqual(first, second):
        if not isinstance(first, Image.Image):
            raise TypeError("first argument is not a PIL Image")
        elif not isinstance(second, Image.Image):
            raise TypeError("second argument is not a PIL Image")
    
        if first.size != second.size:
            return False
            
        w = first.size[0]
        h = first.size[1]
        for x in range(w):
            for y in range(h):
                if first.getpixel((x,y)) != second.getpixel((x,y)):
                    return False
        
        return True
        
def rip_spritesheet(input, width, height, output):
    if not isinstance(input, str):
        raise TypeError("input filename is not a string")
    elif not isinstance(width, int):
        raise TypeError("input filename is not an integer")
    elif not isinstance(height, int):
        raise TypeError("input filename is not an integer")
    elif not isinstance(output, str):
        raise TypeError("output filename is not a string")
    
    orig_sheet = Image.open(input)
    imageCutter = ImageCutter(orig_sheet, width, height)
    imageSet = imageCutter.cut()
    imageSet.create_spritesheet(output) 